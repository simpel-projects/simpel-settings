from django.db import models
from django.utils.translation import gettext_lazy as _

from simpel_settings.models import BaseSetting
from simpel_settings.registries import register_setting


@register_setting
class ExampleSetting(BaseSetting):
    site_name = models.CharField(
        null=True,
        blank=False,
        max_length=255,
        help_text=_("Site name appears on header, footer etc."),
    )
